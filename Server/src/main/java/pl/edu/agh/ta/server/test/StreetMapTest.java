package pl.edu.agh.ta.server.test;


import pl.edu.agh.ta.common.map.AStarAlgorithm;
import pl.edu.agh.ta.common.map.Route;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.ta.common.util.MapDrawer;

// Simple test to check functionalities of AStarAlgorithm and MapDrawer.
public class StreetMapTest {

    public StreetMapTest() {
        StreetMap streetMap = StreetMap.fromResource("/map2.txt");
        Route route = AStarAlgorithm.getShortestRoute(streetMap, 0, 17);
        System.out.println(route);
        System.out.println("Merge:");
        // Test: update the route from junction 6 to go through other junctions
        Route subRoute = new Route(6);
        subRoute.appendJunction(10, 40);
        subRoute.appendJunction(13, 40);
        subRoute.appendJunction(14, 40);
        subRoute.appendJunction(17, 40);
        System.out.println(route.merge(subRoute));
        MapDrawer md = new MapDrawer(streetMap, "~/traffic_ass_logs", "map", 21, 23);
        md.reportPosition(true, 21, 12, 0);
        md.reportPosition(true, 1, 23, 1);
        md.reportPosition(false, 21, 12, 0);
        md.reportPosition(false, 1, 23, 1);
        md.finish();
        md.finish();
    }

    public static void main(String[] args) {
        new StreetMapTest();
    }
}
