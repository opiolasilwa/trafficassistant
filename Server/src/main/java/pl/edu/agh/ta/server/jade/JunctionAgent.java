package pl.edu.agh.ta.server.jade;


import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import pl.edu.agh.ta.common.jade.AbstractMessageFactory;
import pl.edu.agh.ta.common.map.AStarAlgorithm;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.ta.common.util.AbstractLogger;
import pl.edu.agh.ta.common.util.Const;

// Each junction gets a JunctionAgent. They give directions to DriverAgents, based on their knowledge.
public class JunctionAgent extends Agent {
    private AbstractLogger logger;
    private AbstractMessageFactory messageFactory = new ServerMessageFactory();
    private int id;
    private StreetMap streetMap;

    protected void setup() {
        Object[] args = getArguments();
        try {
            logger = (AbstractLogger) args[0];
            id = (int) args[1];
            streetMap = ((StreetMap) args[2]).copy();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Wrong arguments for " + this.getName());
        }

        logger.log("Hello!");
        addBehaviour(new ConversatorBehaviour(this));
    }

    protected void takeDown() {
        logger.log("Bye!");
    }

    class ConversatorBehaviour extends CyclicBehaviour {
        private Agent agent;

        public ConversatorBehaviour(Agent a) {
            super(a);
            agent = a;
        }

        public void action() {
            ACLMessage message = agent.receive();
            if (message == null) {
                block();
            } else {
                // Calculate best route and give directions
                if (message.getProtocol().equals(Const.PROTOCOL_ROUTE_REQUEST)) {
                    int jctTo = Integer.parseInt(message.getContent());
                    ACLMessage response = messageFactory.createRouteResponse(
                            message, AStarAlgorithm.getShortestRoute(streetMap, id, jctTo));
                    agent.send(response);

                // Incorporate info about a certain street into knowledge
                } else if (message.getProtocol().equals(Const.PROTOCOL_OBSTACLE_INFO)) {
                    String[] tokens = message.getContent().split(";");
                    streetMap.setStreet(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));

                // Inform about current cost of requested street
                } else if (message.getProtocol().equals(Const.PROTOCOL_STREET_COST_REQUEST)) {
                    String[] tokens = message.getContent().split(";");
                    ACLMessage response = messageFactory.createStreetCostResponse(message,
                            streetMap.getStreetCost(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1])));
                    agent.send(response);
                }
            }
        }
    }
}
