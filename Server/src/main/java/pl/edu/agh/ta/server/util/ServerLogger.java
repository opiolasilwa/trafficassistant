package pl.edu.agh.ta.server.util;

import pl.edu.agh.ta.common.util.AbstractLogger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

// Custom logger - supports logging  to console and  to file
public class ServerLogger extends AbstractLogger {
    private static final String COMMON_LOG_FILE_NAME = "all_agents";
    private static final Object semaphore = new Object();

    private static boolean logToConsole = true;

    private String id;
    private File logFile;
    private File commonLogFile;

    public ServerLogger(String id, String logDir) {
        this.id = id;
        logDir = logDir.replace("~", System.getProperty("user.home"));
        new File(logDir).mkdirs();
        logFile = new File(makeLogFileName(logDir, id));
        commonLogFile = new File(makeLogFileName(logDir, COMMON_LOG_FILE_NAME));
    }

    public static void setLogToConsole(boolean flag) {
        logToConsole = flag;
    }

    public void log(String message) {
        String logContent = String.format("[%s] (%20s): %s\n",
                new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()),
                id,
                message);
        if (logToConsole) {
            System.out.print(logContent);
        }
        logToFile(logFile, logContent, false);
        logToFile(commonLogFile, logContent, true);
    }

    private String makeLogFileName(String dir, String name) {
        return dir + File.separator + name;
    }

    private void logToFile(File file, String content, boolean synchronous) {
        PrintWriter printWriter = null;
        try {
            if (synchronous) {
                synchronized (semaphore) {
                    printWriter = new PrintWriter(new FileWriter(file, true));
                    printWriter.print(content);
                }
            } else {
                printWriter = new PrintWriter(new FileWriter(file, true));
                printWriter.print(content);
            }
        } catch (IOException e) {
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
}
