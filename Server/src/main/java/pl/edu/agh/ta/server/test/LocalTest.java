package pl.edu.agh.ta.server.test;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import pl.edu.agh.ta.common.jade.DriverAgent;
import pl.edu.agh.ta.common.map.AStarAlgorithm;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.ta.common.util.Const;
import pl.edu.agh.ta.common.util.MapDrawer;
import pl.edu.agh.ta.common.util.Rand;
import pl.edu.agh.ta.server.jade.JunctionAgent;
import pl.edu.agh.ta.server.jade.ObstacleAgent;
import pl.edu.agh.ta.server.jade.ServerMessageFactory;
import pl.edu.agh.ta.server.util.ServerLogger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Properties;

// Test that spawns an ObstacleAgent, JunctionAgents in number according to map size
// and a given number of driving agents. Each agent is hosted on PC.
public class LocalTest {
    private static final int NUMBER_OF_DRIVING_AGENTS = 50;

    public static void main(String[] args) {
        ServerLogger.setLogToConsole(false);

        StreetMap globalMap = StreetMap.fromResource("/map2.txt");

        Properties props = new Properties();
        try {
            props.load(LocalTest.class.getResourceAsStream("/properties.cfg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Profile profile = new ProfileImpl(
                props.getProperty("host", "localhost"),
                Integer.parseInt(props.getProperty("port", "1090")),
                props.getProperty("platform_id", "ta_platform"));
        profile.setParameter("gui", "true");

        String logDir = props.getProperty("log_dir", "~/traffic_ass_logs");
        String timestamp = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss", Locale.US).format(Calendar.getInstance().getTime());
        logDir = logDir + File.separator + timestamp;

        try {
            LinkedList<AgentController> agentControllers = new LinkedList<>();
            AgentController currentAgent;

            AgentContainer container = jade.core.Runtime.instance().createMainContainer(profile);
            container.start();

            Object agentArgs[];
            for (int i = 0; i < globalMap.getNumberOfJunctions(); i++) {
                agentArgs = new Object[]{
                        new ServerLogger(Const.JUNCTION_AGENT_PREFIX + i, logDir),
                        i,
                        globalMap};
                currentAgent = container.createNewAgent(Const.JUNCTION_AGENT_PREFIX + i,
                        JunctionAgent.class.getName(), agentArgs);
                agentControllers.add(currentAgent);
            }

            agentArgs = new Object[]{
                    new ServerLogger("ObstacleAgent", logDir),
                    globalMap,
                    NUMBER_OF_DRIVING_AGENTS};
            currentAgent = container.createNewAgent("ObstacleAgent",
                    ObstacleAgent.class.getName(),
                    agentArgs);
            agentControllers.add(currentAgent);

            for (int i = 0; i < NUMBER_OF_DRIVING_AGENTS; i++) {
                int side = Rand.range(0, 1);
                int jctFrom = 21 + 2 * side + Rand.range(0, 1);
                int jctTo = 23 - 2 * side + Rand.range(0, 1);

                int delay = Rand.range(1, 5) * (Const.TICK_LENGTH_MS) + Rand.range(0, Const.TICK_LENGTH_MS);

                MapDrawer mapDrawer = new MapDrawer(globalMap, logDir, "DrivingAgents" + i, jctFrom, jctTo);
                agentArgs = new Object[]{
                        new ServerLogger("CommunicatingAgent" + i, logDir),
                        mapDrawer,
                        new ServerMessageFactory(),
                        true,
                        jctFrom,
                        jctTo,
                        AStarAlgorithm.getShortestRoute(globalMap, jctFrom, jctTo),
                        delay};
                currentAgent = container.createNewAgent("CommunicatingAgent" + i,
                        DriverAgent.class.getName(),
                        agentArgs);
                agentControllers.add(currentAgent);

                agentArgs = new Object[]{
                        new ServerLogger("DumbAgent" + i, logDir),
                        mapDrawer,
                        new ServerMessageFactory(),
                        false,
                        jctFrom,
                        jctTo,
                        AStarAlgorithm.getShortestRoute(globalMap, jctFrom, jctTo),
                        delay};
                currentAgent = container.createNewAgent("DumbAgent" + i,
                        DriverAgent.class.getName(),
                        agentArgs);
                agentControllers.add(currentAgent);
            }

            for (AgentController agentController : agentControllers) {
                agentController.start();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
