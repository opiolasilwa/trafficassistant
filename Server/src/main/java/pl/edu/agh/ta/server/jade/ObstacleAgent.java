package pl.edu.agh.ta.server.jade;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import pl.edu.agh.ta.common.map.StreetInfo;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.ta.common.util.AbstractLogger;
import pl.edu.agh.ta.common.util.Const;
import pl.edu.agh.ta.common.util.Rand;
import pl.edu.agh.ta.common.util.TrafficGenerator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Created by lopiola on 13.05.14.
 */
// At one instance per simulation, this agent generates street events, calculates traffic and informs
// Junction agents
public class ObstacleAgent extends Agent {
    private AbstractLogger logger;
    private ServerMessageFactory messageFactory = new ServerMessageFactory();
    private StreetMap globalMap;
    private StreetMap basicMap;
    private TrafficGenerator trafficGenerator;

    private int numberOfDriverAgents;

    private int ticker;

    // All streets on the map
    private LinkedList<StreetInfo> streets;
    private Map<Integer, Map<Integer, StreetInfo>> agentsOnStreetsMap;
    // Knowledge of junction agents
    private LinkedList<StreetInfo>[] agentKnowledge;
    // Which agents know given street
    private HashMap<StreetInfo, LinkedList<Integer>> agentsByStreet;

    protected void setup() {
        Object[] args = getArguments();
        try {
            logger = (AbstractLogger) args[0];
            globalMap = (StreetMap) args[1];
            basicMap = globalMap.copy();
            numberOfDriverAgents = (int) args[2] * 2; // *2 because there is a communicating and dumb agent
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Wrong arguments for " + this.getName());
        }
        ticker = 0;
        trafficGenerator = new TrafficGenerator();
        streets = new LinkedList<>(globalMap.getAllStreets());
        agentsOnStreetsMap = new HashMap<>();
        for (StreetInfo streetInfo : streets) {
            if (!agentsOnStreetsMap.containsKey(streetInfo.getJctFrom())) {
                agentsOnStreetsMap.put(streetInfo.getJctFrom(), new HashMap<Integer, StreetInfo>());
            }
            agentsOnStreetsMap.get(streetInfo.getJctFrom()).put(streetInfo.getJctTo(), streetInfo);
        }

        calculateAgentsKnowledge();
        logger.log("Hello!");
        addBehaviour(new ObstacleGeneratorBehaviour(this));
    }

    // Calculate knowledge of Junction Agents, each knows about a subgraph of city map
    // (all streets not further than JUNCTION_AGENT_RANGE, but with some omitted - MISSING_KNOWLEDGE_SIZE)
    private void calculateAgentsKnowledge() {
        agentsByStreet = new HashMap<>();
        agentKnowledge = new LinkedList[globalMap.getNumberOfJunctions()];
        for (int i = 0; i < globalMap.getNumberOfJunctions(); i++) {
            agentKnowledge[i] = globalMap.subGraph(i, Const.JUNCTION_AGENT_RANGE, Const.MISSING_KNOWLEDGE_SIZE);
            StringBuilder sb = new StringBuilder(Const.JUNCTION_AGENT_PREFIX + i + "'s knowledge is: ");
            for (StreetInfo streetInfo : agentKnowledge[i]) {
                sb.append("[");
                sb.append(streetInfo);
                sb.append("], ");
            }
            sb.delete(sb.length() - 2, sb.length());
            logger.log(sb.toString());

            for (StreetInfo streetInfo : agentKnowledge[i]) {
                if (!agentsByStreet.containsKey(streetInfo)) {
                    agentsByStreet.put(streetInfo, new LinkedList<Integer>());
                }
                agentsByStreet.get(streetInfo).add(i);
            }
        }
    }


    class ObstacleGeneratorBehaviour extends CyclicBehaviour {
        private Agent agent;
        private PriorityQueue<PlannedEvent> events = new PriorityQueue<>();
        int nextEvent = 1;
        long lastTick = 0;

        public ObstacleGeneratorBehaviour(Agent a) {
            super(a);
            agent = a;
        }

        // Every now and then create problems on streets and remove old problems
        public void action() {
            performPlannedEvents();

            ACLMessage message;
            while ((message = receive()) != null) {
                if (message.getProtocol().equals(Const.PROTOCOL_ROUTE_FINISHED_INFO)) {
                    System.out.println("Another agent finished his route. " + (numberOfDriverAgents - 1) + " left.");
                    if (--numberOfDriverAgents == 0) {
                        logger.log("All agents have finished their routes.");
                        System.exit(0);
                    }
                } else {
                    // Enter / Exit street info
                    String[] tokens = message.getContent().split(";");
                    int jctFrom = Integer.valueOf(tokens[0]);
                    int jctTo = Integer.valueOf(tokens[1]);
                    if (message.getProtocol().equals(Const.PROTOCOL_ENTER_STREET_INFO)) {
                        agentsOnStreetsMap.get(jctFrom).get(jctTo).addAgentToStreet();
                        logger.log("Add agent to street " + jctFrom + " -> " + jctTo);
                    } else if (message.getProtocol().equals(Const.PROTOCOL_EXIT_STREET_INFO)) {
                        agentsOnStreetsMap.get(jctFrom).get(jctTo).removeAgentFromStreet();
                        logger.log("Remove agent from street " + jctFrom + " -> " + jctTo);
                    }
                }
            }

            if (System.currentTimeMillis() - lastTick > Const.TICK_LENGTH_MS) {
                lastTick = System.currentTimeMillis();
                ticker++;

                //Set street cost by trafficgenerator only if the street is not closed
                for (StreetInfo streetInfo : streets) {
                    int oldCost = globalMap.getStreetCost(streetInfo.getJctFrom(), streetInfo.getJctTo());
                    if (oldCost != 0) {
                        int newCost = basicMap.getStreetCost(streetInfo.getJctFrom(), streetInfo.getJctTo());
                        newCost *= trafficGenerator.getTrafficMultiplier(ticker + (streetInfo.getJctFrom() * streetInfo.getJctTo())) +
                                agentsOnStreetsMap.get(streetInfo.getJctFrom()).get(streetInfo.getJctTo()).getCountOfAgentsOnStreet() *
                                        Const.AGENT_IMPACT_ON_TRAFFIC;
                        globalMap.setStreet(streetInfo.getJctFrom(), streetInfo.getJctTo(), newCost);
                        if (agentsByStreet.get(streetInfo) != null) {
                            message = messageFactory.createObstacleInfo(streetInfo, newCost, agentsByStreet.get(streetInfo));
                            agent.send(message);
                        }
                    }
                }

                //only close streets
                if (nextEvent <= ticker) {
                    StreetInfo newEventStreet = randomStreet();
                    //StreetInfo newEventStreet = agentsOnStreetsMap.get(3).get(6); // for test purposes
                    if (newEventStreet != null) {
                        int duration = Rand.range(Const.MIN_EVENT_DURATION, Const.MAX_EVENT_DURATION);
                        if (Rand.takeChance(Const.STREET_CLOSING_CHANCE)) {
                            logger.log("[" + newEventStreet + "] is getting closed for " + duration);
                            globalMap.setStreet(newEventStreet.getJctFrom(), newEventStreet.getJctTo(), 0);
                            message = messageFactory.createObstacleInfo(newEventStreet, 0, agentsByStreet.get(newEventStreet));
                            agent.send(message);
                            events.add(new PlannedEvent(ticker + duration, newEventStreet));
                            nextEvent = ticker + Rand.range(Const.MIN_EVENT_PERIOD, Const.MAX_EVENT_PERIOD);
                        }
                        // Uncomment for testing
//                    if (ticker == 1){
//                        newCost = 0;
//                        newEventStreet= new StreetInfo(2, 3);
//                        duration = 15;
//                    }


                    }
                }
            }

            // yield
            doWait(1);
        }

        // Randoms a street, that is currently free of any events. Tries up to 5 times and gives up.
        private StreetInfo randomStreet() {
            int i = 0;
            StreetInfo randomedStreet;
            do {
                randomedStreet = streets.get(Rand.uniform(streets.size()));
            }
            while (events.contains(new PlannedEvent(0, randomedStreet)) && i++ < 5);
            if (i < 5) {
                return randomedStreet;
            } else {
                return null;
            }
        }

        // Finished events that were scheduled to finish now
        private void performPlannedEvents() {
            PlannedEvent event = events.peek();
            while (event != null && event.dueTime <= ticker) {
                StreetInfo streetInfo = event.streetInfo;
                int newCost = basicMap.getStreetCost(streetInfo.getJctFrom(), streetInfo.getJctTo());
                globalMap.setStreet(streetInfo.getJctFrom(), streetInfo.getJctTo(), newCost);
                logger.log("[" + streetInfo + "] is now passable");
                ACLMessage message = messageFactory.createObstacleInfo(
                        streetInfo,
                        newCost,
                        agentsByStreet.get(streetInfo));
                agent.send(message);

                events.poll();
                event = events.peek();
            }
        }
    }

    // Class to represent events in priority queue, so they can be sorted by due time
    class PlannedEvent implements Comparable<PlannedEvent> {
        public int dueTime;
        public StreetInfo streetInfo;

        PlannedEvent(int dueTime, StreetInfo streetInfo) {
            this.dueTime = dueTime;
            this.streetInfo = streetInfo;
        }

        @Override
        public int compareTo(PlannedEvent o) {
            return Integer.compare(dueTime, o.dueTime);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PlannedEvent that = (PlannedEvent) o;

            if (streetInfo != null ? !streetInfo.equals(that.streetInfo) : that.streetInfo != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return streetInfo != null ? streetInfo.hashCode() : 0;
        }
    }
}
