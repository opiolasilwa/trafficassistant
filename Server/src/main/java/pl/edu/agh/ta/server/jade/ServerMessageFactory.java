package pl.edu.agh.ta.server.jade;


import jade.core.AID;
import jade.lang.acl.ACLMessage;
import pl.edu.agh.ta.common.map.Route;
import pl.edu.agh.ta.common.map.StreetInfo;
import pl.edu.agh.ta.common.jade.AbstractMessageFactory;
import pl.edu.agh.ta.common.util.Const;

import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by Adiki on 30.04.14.
 */
// Implementation of message factory to be used by PC agents
public class ServerMessageFactory extends AbstractMessageFactory {
    @Override
    public ACLMessage createRouteRequest(int jctFrom, int jctTo) {
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setProtocol(Const.PROTOCOL_ROUTE_REQUEST);
        request.setContent(jctTo + "");
        AID r = new AID(Const.JUNCTION_AGENT_PREFIX + jctFrom, AID.ISLOCALNAME);
        request.addReceiver(r);
        request.setReplyWith(Const.PROTOCOL_ROUTE_REQUEST + System.currentTimeMillis());
        return request;
    }

    @Override
    public ACLMessage createRouteResponse(ACLMessage request, Route route) {
        ACLMessage response = request.createReply();
        response.setPerformative(ACLMessage.INFORM);
        try {
            response.setContentObject(route);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public ACLMessage createStreetCostRequest(int jctFrom, int jctTo) {
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.setProtocol(Const.PROTOCOL_STREET_COST_REQUEST);
        request.setContent(jctFrom + ";" + jctTo);
        AID r = new AID(Const.JUNCTION_AGENT_PREFIX + jctFrom, AID.ISLOCALNAME);
        request.addReceiver(r);
        request.setReplyWith(Const.PROTOCOL_STREET_COST_REQUEST + System.currentTimeMillis());
        return request;
    }

    @Override
    public ACLMessage createStreetCostResponse(ACLMessage request, int cost) {
        ACLMessage response = request.createReply();
        response.setPerformative(ACLMessage.INFORM);
        response.setContent(cost + "");
        return response;
    }

    @Override
    public ACLMessage createEnterStreetInfoMessage(int jctFrom, int jctTo) {
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setProtocol(Const.PROTOCOL_ENTER_STREET_INFO);
        message.setContent(String.format("%d;%d", jctFrom, jctTo));
        message.addReceiver(new AID("ObstacleAgent", AID.ISLOCALNAME));
        return message;
    }

    @Override
    public ACLMessage createExitStreetInfoMessage(int jctFrom, int jctTo) {
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setProtocol(Const.PROTOCOL_EXIT_STREET_INFO);
        message.setContent(String.format("%d;%d", jctFrom, jctTo));
        message.addReceiver(new AID("ObstacleAgent", AID.ISLOCALNAME));
        return message;
    }

    public ACLMessage createObstacleInfo(StreetInfo streetInfo, int newCost, LinkedList<Integer> recipients) {
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setProtocol(Const.PROTOCOL_OBSTACLE_INFO);
        message.setContent(String.format("%d;%d;%d", streetInfo.getJctFrom(), streetInfo.getJctTo(), newCost));
        if (recipients != null) {
            for (Integer i : recipients) {
                AID r = new AID(Const.JUNCTION_AGENT_PREFIX + i, AID.ISLOCALNAME);
                message.addReceiver(r);
            }
        }
        return message;
    }

    @Override
    public ACLMessage createRouteFinishedInfo() {
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.setProtocol(Const.PROTOCOL_ROUTE_FINISHED_INFO);
        message.addReceiver(new AID("ObstacleAgent", AID.ISLOCALNAME));
        return message;
    }
}
