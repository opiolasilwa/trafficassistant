package pl.edu.agh.ta.server.test;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import pl.edu.agh.ta.common.map.StreetMap;
import pl.edu.agh.ta.common.util.Const;
import pl.edu.agh.ta.server.jade.JunctionAgent;
import pl.edu.agh.ta.server.jade.ObstacleAgent;
import pl.edu.agh.ta.server.util.ServerLogger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

/**
 * Created by lopiola on 13.05.14.
 */

// Test that spawns an ObstacleAgent and JunctionAgents in number according to map size.
// Driving agents can be added for example from mobile container.
public class TestNoDriver {
    private static final int NUMBER_OF_DRIVING_AGENTS = 1;

    public static void main(String[] args) {

        StreetMap globalMap = StreetMap.fromResource("/map1.txt");

        Properties props = new Properties();
        try {
            props.load(LocalTest.class.getResourceAsStream("/properties.cfg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Profile profile = new ProfileImpl(
                props.getProperty("host", "localhost"),
                Integer.parseInt(props.getProperty("port", "1090")),
                props.getProperty("platform_id", "ta_platform"));

        profile.setParameter("gui", "true");

        String logDir = props.getProperty("log_dir", "~/traffic_ass_logs");
        String timestamp = new SimpleDateFormat("yyyy_mm_dd-HH_mm_ss", Locale.US).format(Calendar.getInstance().getTime());
        logDir = logDir + File.separator + timestamp;

        try {
            AgentContainer container = jade.core.Runtime.instance().createMainContainer(profile);
            container.start();

            Object agentArgs[];
            for (int i = 0; i < globalMap.getNumberOfJunctions(); i++) {
                agentArgs = new Object[]{
                        new ServerLogger(Const.JUNCTION_AGENT_PREFIX + i, logDir),
                        i,
                        globalMap};
                container.createNewAgent(Const.JUNCTION_AGENT_PREFIX + i,
                        JunctionAgent.class.getName(), agentArgs).start();
            }

            agentArgs = new Object[]{
                    new ServerLogger("ObstacleAgent", logDir),
                    globalMap,
                    NUMBER_OF_DRIVING_AGENTS};
            container.createNewAgent("ObstacleAgent",
                    ObstacleAgent.class.getName(),
                    agentArgs).start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
