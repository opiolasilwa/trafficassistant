package pl.edu.agh.ta.common.jade;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import pl.edu.agh.ta.common.map.*;
import pl.edu.agh.ta.common.util.AbstractLogger;
import pl.edu.agh.ta.common.util.Const;
import pl.edu.agh.ta.common.util.MapDrawer;

// Main agent in simulation - tries to reach some destination from starting point.
// Can be one of two types:
// - communicating (asks JunctionAgents for directions)
// - non-communicating (uses default route and is indifferent to any street events)
public class DriverAgent extends Agent {
    private AbstractLogger logger;
    private AbstractMessageFactory requestFactory;
    private MapDrawer mapDrawer;

    // Does this agents communicate to ask for directions
    private boolean communicates;

    // How often is next step performed in ms
    // The agent covers this number of units of distance in one tick

    // The starting and ending junction of route to cover
    private int routeFrom;
    private int routeTo;

    private Route currentRoute;

    // Positioning on map
    private int currentCheckpoint;
    private int currentStreetStart;
    private int currentStreetEnd;
    private int currentStreetCost;
    private double currentPosition;

    // How many ticks did the drive take
    private int totalTicks = 0;

    protected void setup() {
        int delay;
        Object[] args = getArguments();
        try {
            logger = (AbstractLogger) args[0];
            mapDrawer = (MapDrawer) args[1];
            requestFactory = (AbstractMessageFactory) args[2];
            communicates = (boolean) args[3];
            routeFrom = (int) args[4];
            routeTo = (int) args[5];
            currentRoute = (Route) args[6];
            delay = (int) args[7];
        } catch (Exception e) {
            throw new RuntimeException("Wrong arguments for " + this.getName());
        }

        // Wait given delay, so all agents don't start simultaneously
        // delay is provided in agent args
        doWait(delay);

        logger.log("Hello!");
        logger.log("My speed is " + Const.AGENT_SPEED + " / " + Const.TICK_LENGTH_MS + "ms");
        logger.log(String.format("I'm trying to get from junction %d -> %d", routeFrom, routeTo));
        logger.log("Found shortest route: " + currentRoute);

        // Start driving
        currentCheckpoint = 0;
        currentStreetStart = routeFrom;
        currentStreetEnd = currentRoute.getRouteCheckpointAt(currentCheckpoint);
        send(requestFactory.createEnterStreetInfoMessage(currentStreetStart, currentStreetEnd));
        addBehaviour(new IntelligentDriverBehaviour(this));
    }

    protected void takeDown() {
        logger.log("Bye!");
    }

    private static String streetToString(int from, int to) {
        return from + " -> " + to;
    }

    class IntelligentDriverBehaviour extends CyclicBehaviour {
        private Agent agent;

        public IntelligentDriverBehaviour(Agent a) {
            super(a);
            agent = a;
        }

        public void action() {
            // report current position to MapDrawer
            if (mapDrawer != null) {
                mapDrawer.reportPosition(communicates, currentStreetStart, currentStreetEnd, currentPosition);
            }
            // If current street is cosed, wait
            if (currentStreetCost == 0) {
                logger.log("Not moving...");
                currentStreetCost = askForStreetCost();
            } else {
                // Report current position to logs
                logger.log(String.format("Covered %5.2f%% of %s.",
                        currentPosition * 100,
                        streetToString(currentStreetStart, currentStreetEnd)));

                boolean approachingJunction = currentPosition + (Const.AGENT_SPEED + .0) / currentStreetCost >= 1;

                // If not close to junction, just move
                if (!approachingJunction) {
                    currentPosition += (Const.AGENT_SPEED + .0) / currentStreetCost;

                } else { // Check which street to choose
                    // Maybe the destination will be reached in this step?
                    if (currentStreetEnd == routeTo) {
                        totalTicks++;
                        doWait(Const.TICK_LENGTH_MS);
                        logger.log("Destination reached in " + totalTicks + " ticks.");
                        agent.send(requestFactory.createExitStreetInfoMessage(currentStreetStart, currentStreetEnd));
                        if (mapDrawer != null) {
                            mapDrawer.reportPosition(communicates, currentStreetStart, currentStreetEnd, 1);
                            mapDrawer.finish();
                        }
                        agent.send(requestFactory.createRouteFinishedInfo());
                        doDelete();
                    } else {
                        // Next street to turn into, according to current route
                        currentCheckpoint++;

                        if (communicates) {
                            askForRoute();
                        }

                        // Get information about next street, set position to zero
                        agent.send(requestFactory.createExitStreetInfoMessage(currentStreetStart, currentStreetEnd));
                        currentStreetStart = currentStreetEnd;
                        currentStreetEnd = currentRoute.getRouteCheckpointAt(currentCheckpoint);
                        currentStreetCost = askForStreetCost();
                        currentPosition = 0;
                        agent.send(requestFactory.createEnterStreetInfoMessage(currentStreetStart, currentStreetEnd));
                        logger.log("Entering street " + streetToString(currentStreetStart, currentStreetEnd));

                    }
                }
            }
            // Wait time of a tick
            totalTicks++;
            doWait(Const.TICK_LENGTH_MS);
        }

        // Ask JunctionAgent for directions when approaching a junction
        private void askForRoute() {
            logger.log("Asking agent at junction " + currentStreetEnd + " for directions");
            ACLMessage request = requestFactory.createRouteRequest(currentStreetEnd, routeTo);
            agent.send(request);
            ACLMessage response = agent.blockingReceive();
            Route newRoute = null;
            try {
                newRoute = (Route) response.getContentObject();
            } catch (UnreadableException e) {
                e.printStackTrace();
            }

            // Streets are closed, so the target junction is unreachable
            // Don't change the route then
            if (newRoute != null) {
                currentRoute = currentRoute.merge(newRoute);
                logger.log("Got new route: " + currentRoute.toString(currentCheckpoint - 1));
            } else {
                logger.log("My target is unreachable. Staying on current route.");
            }
        }

        // Ask JunctionAgent about current street cost
        private int askForStreetCost() {
            ACLMessage request = requestFactory.createStreetCostRequest(currentStreetStart, currentStreetEnd);
            agent.send(request);
            ACLMessage response = agent.blockingReceive();
            int result = Integer.parseInt(response.getContent());
            if (result < 0) {
                logger.log("WARNING: Received negative street cost: " + result);
                result = 0;
            }
            return result;
        }
    }
}
