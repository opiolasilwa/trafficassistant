package pl.edu.agh.ta.common.map;

import java.io.*;
import java.util.*;

// A street map holding a graph of streets and info about their cost
public class StreetMap {
    private int numberOfJunctions;

    // Location in (x, y) coord space
    private Point[] junctionLocations;

    // streets[0][3] = 5 means, that there is a street
    // connecting junction 3 to 5 (one way road), that has cost = 5
    // If cost = 0, there is no connection
    private int[][] streets;

    public StreetMap(int numberOfJunctions) {
        this.numberOfJunctions = numberOfJunctions;
        junctionLocations = new Point[numberOfJunctions];
        streets = new int[numberOfJunctions][numberOfJunctions];
        for (int i = 0; i < numberOfJunctions; i++) {
            for (int j = 0; j < numberOfJunctions; j++) {
                streets[i][j] = 0;
            }
        }
    }

    // Sets location of a junction in (x, y) coord space
    public void setIntersectionLocation(int jctNum, Point location) {
        junctionLocations[jctNum] = location;
    }

    // Sets a street from a to b with default cost (distance in (x, y) coord space * 10)
    public void setStreet(int jctFrom, int jctTo) {
        Point from = junctionLocations[jctFrom];
        Point to = junctionLocations[jctTo];
        int cost = (int) Math.round(from.distance(to) * 10);
        setStreet(jctFrom, jctTo, cost);
    }

    // Sets a street from a to b to have given cost
    public void setStreet(int jctFrom, int jctTo, int cost) {
        streets[jctFrom][jctTo] = cost;
    }

    public int getNumberOfJunctions() {
        return numberOfJunctions;
    }

    public Point getJunctionLocation(int jctNum) {
        return junctionLocations[jctNum];
    }

    public int getStreetCost(int jctFrom, int jctTo) {
        return streets[jctFrom][jctTo];
    }

    public List<Integer> getNeighbours(int id) {
        List<Integer> result = new LinkedList<Integer>();
        for (int i = 0; i < numberOfJunctions; i++) {
            if (streets[id][i] > 0) {
                result.add(i);
            }
        }
        return result;
    }


    public StreetMap copy() {
        StreetMap result = new StreetMap(numberOfJunctions);
        for (int i = 0; i < numberOfJunctions; i++) {
            result.junctionLocations[i] = this.junctionLocations[i];
            for (int j = 0; j < numberOfJunctions; j++) {
                result.streets[i][j] = this.streets[i][j];
            }
        }
        return result;
    }

    public Collection<StreetInfo> getAllStreets() {
        Collection<StreetInfo> result = new LinkedList<>();
        for (int i = 0; i < numberOfJunctions; i++) {
            for (int j = 0; j < numberOfJunctions; j++) {
                if (streets[i][j] > 0) {
                    result.add(new StreetInfo(i, j));
                }
            }
        }
        return result;
    }

    // Calculate knowledge of a single JunctionAgent
    public LinkedList<StreetInfo> subGraph(int junction, int range, int missingKnowledgeSize) {
        LinkedList<StreetInfo> result = new LinkedList<>();
        LinkedList<StreetInfo> maxRangeStreets = new LinkedList<>();
        int distances[] = new int[numberOfJunctions];
        Arrays.fill(distances, -1);
        distances[junction] = 0;
        for (int distance = 0; distance < range; distance++) {
            for (int i = 0; i < numberOfJunctions; i++) {
                if (distances[i] == distance) {
                    for (int j = 0; j < numberOfJunctions; j++) {
                        if (streets[i][j] > 0) {
                            if (distances[j] == -1) {
                                distances[j] = distance + 1;
                            }
                            StreetInfo streetInfo = new StreetInfo(i, j);
                            if (!result.contains(streetInfo)) {
                                result.add(streetInfo);
                            }
                            if (distance == range - 1 && !maxRangeStreets.contains(streetInfo)) {
                                maxRangeStreets.add(streetInfo);
                            }
                        }
                    }
                }
            }
        }

        // Remove couple of streets at max range
        // But leave at least one
        while (missingKnowledgeSize > 0 && maxRangeStreets.size() > 1) {
            result.remove(new Random().nextInt(result.size()));
            missingKnowledgeSize--;
        }
        return result;
    }

    public static StreetMap fromResource(String resourceName) {
        try {
            String[] fileContent = readLines(resourceName);
            // Get number of junctions - first number in file
            int numberOfJunctions = Integer.parseInt(fileContent[0]);
            StreetMap result = new StreetMap(numberOfJunctions);

            // Next numberOfJunctions lines are coords of junctions
            for (int i = 0; i < numberOfJunctions; i++) {
                String currentLine = fileContent[i + 1];
                String[] nums = currentLine.split(" ");
                Point location = new Point(Integer.parseInt(nums[0]), Integer.parseInt(nums[1]));
                result.setIntersectionLocation(i, location);
                //System.out.println("New junction: " + location);
            }

            // Next numberOfJunctions lines are existing street connections to other junctions
            for (int i = 0; i < numberOfJunctions; i++) {
                String currentLine = fileContent[i + numberOfJunctions + 1];
                String[] nums = currentLine.split(" ");
                for (String jct : nums) {
                    result.setStreet(i, Integer.parseInt(jct));
                    //System.out.println("New street: " + i + " -> " + Integer.parseInt(jct));
                }
            }

            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String[] readLines(String resourceName) throws IOException {
        InputStream is = StreetMap.class.getResourceAsStream(resourceName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        List<String> lines = new ArrayList<String>();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }
        bufferedReader.close();
        return lines.toArray(new String[lines.size()]);
    }
}
