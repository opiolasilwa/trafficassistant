package pl.edu.agh.ta.common.util;

import java.util.Random;

/**
 * Created by lopiola on 13.05.14.
 */
// some convenience functions for number randomization
public class Rand {
    private static Random random = new Random();

    public static boolean takeChance(double chance) {
        return random.nextDouble() <= chance;
    }

    public static int uniform(int max) {
        return random.nextInt(max);
    }

    public static double uniform(double max) {
        return random.nextInt() * max;
    }

    public static int range(int from, int to) {
        return random.nextInt(to - from + 1) + from;
    }

    public static double range(double from, double to) {
        return from + random.nextDouble() * (to - from);
    }
}