package pl.edu.agh.ta.common.util;


// Lets PC agents and mobile agents have distinct logging implementations
public abstract class AbstractLogger {
    public abstract void log(String message);
}
