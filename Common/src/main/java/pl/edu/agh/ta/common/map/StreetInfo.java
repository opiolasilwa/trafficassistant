package pl.edu.agh.ta.common.map;

// Represents a single street segment
public class StreetInfo {
    private int jctFrom;
    private int jctTo;
    private int countOfAgentsOnStreet;

    public StreetInfo(int jctFrom, int jctTo) {
        this.jctFrom = jctFrom;
        this.jctTo = jctTo;
        countOfAgentsOnStreet = 0;
    }

    public void addAgentToStreet() {
        ++countOfAgentsOnStreet;
    }

    public void removeAgentFromStreet() {
        --countOfAgentsOnStreet;
    }

    public int getCountOfAgentsOnStreet() {
        return countOfAgentsOnStreet;
    }

    public int getJctFrom() {
        return jctFrom;
    }

    public int getJctTo() {
        return jctTo;
    }

    @Override
    public String toString() {
        return String.format("%2d -> %2d", jctFrom, jctTo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StreetInfo that = (StreetInfo) o;

        if (jctFrom != that.jctFrom) return false;
        if (jctTo != that.jctTo) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = jctFrom;
        result = 31 * result + jctTo;
        return result;
    }
}
