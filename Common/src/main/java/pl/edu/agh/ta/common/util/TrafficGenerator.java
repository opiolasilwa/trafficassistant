package pl.edu.agh.ta.common.util;

/**
 * Created by Adiki on 2014-06-01.
 */
// Generates a sine-like fluctuations of traffic
public class TrafficGenerator {
    double a, b, c;

    public TrafficGenerator() {
        a = Math.random() / 100 + 0.01;
        b = Math.random() / 20 + 0.1;
        c = Math.random() / 20  + 0.5;
    }

    public double getTrafficMultiplier(double x) {
        return 1 + (Math.sin(a * x) + Math.sin(b * (x + 1)) + Math.sin(c * (x + 2)) + 3) / 6 * Const.MAX_TRAFFIC_GAIN;
    }

    public static void main(String [] args) {
        TrafficGenerator trafficGenerator = new TrafficGenerator();
        for (int i = 0; i < 100; ++i) {
            System.out.println(trafficGenerator.getTrafficMultiplier(i));
        }
    }
}
