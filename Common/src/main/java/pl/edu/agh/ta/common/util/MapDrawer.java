package pl.edu.agh.ta.common.util;

import pl.edu.agh.ta.common.map.*;
import pl.edu.agh.ta.common.map.Point;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by lopiola on 23.05.14.
 */

// Allows drawing a street map with agent routes on it. One instance per two agents (communicating and non-communicating)
public class MapDrawer {
    private int imageWidth = 1300;
    private int imageHeight = 700;

    private StreetMap streetMap;
    private String targetDir;
    private String id;

    int jctFrom;
    int jctTo;

    // list of positions of agents
    private LinkedList<Point> commAgentPositions;
    private LinkedList<Point> nonCommAgentPositions;

    public MapDrawer(StreetMap streetMap, String targetDir, String id, int jctFrom, int jctTo) {
        this.streetMap = streetMap.copy();
        this.targetDir = targetDir.replace("~", System.getProperty("user.home"));
        new File(this.targetDir).mkdirs();
        this.id = id;

        this.jctFrom = jctFrom;
        this.jctTo = jctTo;

        this.commAgentPositions = new LinkedList<>();
        this.nonCommAgentPositions = new LinkedList<>();
    }

    // Collects sequential positions of agents to be drawn
    public void reportPosition(boolean communicatingAgent, int jctFrom, int jctTo, double positionOnStreet) {
        double posX = streetMap.getJunctionLocation(jctFrom).getX() +
                (streetMap.getJunctionLocation(jctTo).getX() - streetMap.getJunctionLocation(jctFrom).getX()) * positionOnStreet;
        double posY = streetMap.getJunctionLocation(jctFrom).getY() +
                (streetMap.getJunctionLocation(jctTo).getY() - streetMap.getJunctionLocation(jctFrom).getY()) * positionOnStreet;
        if (communicatingAgent) {
            commAgentPositions.add(new Point(posX, posY));
        } else {
            nonCommAgentPositions.add(new Point(posX, posY));
        }
    }

    private int numAgentsFinished = 0;

    // If two agents have finished, draws the map with their routes on it, and saves it to file
    public void finish() {
        numAgentsFinished++;
        if (numAgentsFinished == 2) {
            MapDrawerImage image = new MapDrawerImage();
            try {
                ImageIO.write(image, "png", new File(targetDir + File.separator + id + ".png"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Performs the drawing of image
    public class MapDrawerImage extends BufferedImage {
        private int minX, minY, maxX, maxY, dx, dy;

        public MapDrawerImage() {
            super(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = createGraphics();

            minX = (int) streetMap.getJunctionLocation(0).getX();
            minY = (int) streetMap.getJunctionLocation(0).getY();
            maxX = (int) streetMap.getJunctionLocation(0).getX();
            maxY = (int) streetMap.getJunctionLocation(0).getY();

            for (int i = 0; i < streetMap.getNumberOfJunctions(); ++i) {
                Point point = streetMap.getJunctionLocation(i);
                if (point.getX() > maxX) maxX = (int) point.getX();
                if (point.getY() > maxY) maxY = (int) point.getY();
                if (point.getX() < minX) minX = (int) point.getX();
                if (point.getY() < minY) minY = (int) point.getY();
            }

            dx = maxX - minX;
            dy = maxY - minY;


            g2.setPaint(Color.white);
            g2.fillRect(0, 0, imageWidth, imageHeight);

            g2.setPaint(Color.gray);
            g2.setStroke(new BasicStroke(3));
            for (StreetInfo streetInfo : streetMap.getAllStreets()) {
                int x1, x2, y1, y2;
                x1 = calcX(streetMap.getJunctionLocation(streetInfo.getJctFrom()).getX()) + 5;
                y1 = calcY(streetMap.getJunctionLocation(streetInfo.getJctFrom()).getY()) + 5;
                x2 = calcX(streetMap.getJunctionLocation(streetInfo.getJctTo()).getX()) + 5;
                y2 = calcY(streetMap.getJunctionLocation(streetInfo.getJctTo()).getY()) + 5;
                g2.drawLine(x1, y1, x2, y2);
            }

            g2.setPaint(Color.black);
            for (int i = 0; i < streetMap.getNumberOfJunctions(); i++) {
                Point junction = streetMap.getJunctionLocation(i);
                int x = calcX(junction.getX());
                int y = calcY(junction.getY());
                g2.fillOval(x, y, 10, 10);
            }

            // Communicating agent
            // Draw route
            Point firstPoint = commAgentPositions.removeFirst();
            Point lastPoint = firstPoint;
            g2.setPaint(new Color(50, 255, 100));
            for (Point currentPoint : commAgentPositions) {
                g2.drawLine(calcX(lastPoint.getX()), calcY(lastPoint.getY()) - 3, calcX(currentPoint.getX()), calcY(currentPoint.getY()) - 3);
                lastPoint = currentPoint;
            }

            // Draw reported points
            g2.setPaint(new Color(40, 140, 60));
            g2.fillOval(calcX(firstPoint.getX()) - 4, calcY(firstPoint.getY()) - 7, 8, 8);
            for (Point currentPoint : commAgentPositions) {
                g2.fillOval(calcX(currentPoint.getX()) - 4, calcY(currentPoint.getY()) - 7, 8, 8);
            }
            commAgentPositions.addFirst(firstPoint);

            g2.setFont(new Font("Cambria", Font.PLAIN, 24));
            g2.drawString(Integer.toString(commAgentPositions.size()),
                    imageWidth - 100,
                    50);

            // Non-communicating agent
            // Draw route
            firstPoint = nonCommAgentPositions.removeFirst();
            lastPoint = firstPoint;
            g2.setPaint(new Color(255, 80, 70));
            for (Point currentPoint : nonCommAgentPositions) {
                g2.drawLine(calcX(lastPoint.getX()), calcY(lastPoint.getY()) + 3, calcX(currentPoint.getX()), calcY(currentPoint.getY()) + 3);
                lastPoint = currentPoint;
            }

            // Draw reported points
            g2.setPaint(new Color(190, 60, 35));
            g2.fillOval(calcX(firstPoint.getX()) - 4, calcY(firstPoint.getY()) - 1, 8, 8);
            for (Point currentPoint : nonCommAgentPositions) {
                g2.fillOval(calcX(currentPoint.getX()) - 4, calcY(currentPoint.getY()) - 1, 8, 8);
            }
            nonCommAgentPositions.addFirst(firstPoint);

            g2.setFont(new Font("Cambria", Font.PLAIN, 24));
            g2.drawString(Integer.toString(nonCommAgentPositions.size()),
                    imageWidth - 50,
                    50);

            // Route info
            g2.setPaint(Color.black);
            g2.setFont(new Font("Cambria", Font.PLAIN, 24));
            g2.drawString(jctFrom + " -> " + jctTo,
                    imageWidth - 250,
                    50);

            // Start, finish points
            g2.setPaint(Color.gray);
            g2.fillRect(calcX(commAgentPositions.getFirst().getX()) - 16, calcY(commAgentPositions.getFirst().getY()) - 16, 32, 32);
            g2.fillRect(calcX(commAgentPositions.getLast().getX()) - 16, calcY(commAgentPositions.getLast().getY()) - 16, 32, 32);
            g2.setPaint(Color.black);
            g2.drawString("S", calcX(commAgentPositions.getFirst().getX()) - 8, calcY(commAgentPositions.getFirst().getY()) + 8);
            g2.drawString("F", calcX(commAgentPositions.getLast().getX()) - 7, calcY(commAgentPositions.getLast().getY()) + 8);
        }

        private int calcX(double x) {
            return (int) ((x - minX) / dx * (imageWidth - 70) + 30);
        }

        private int calcY(double y) {
            return (int) ((y - minY) / dy * (imageHeight - 70) + 30);
        }
    }
}
