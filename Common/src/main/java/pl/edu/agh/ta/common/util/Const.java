package pl.edu.agh.ta.common.util;

/**
 * Created by lopiola on 13.05.14.
 */
public class Const {
    // Length in ms of every simulation tick
    public static final int TICK_LENGTH_MS = 200;

    // Amount of distance covered in one tick by agents
    public static final int AGENT_SPEED = 10;

    // JuntionsAgents' names will be prefix + number
    public static final String JUNCTION_AGENT_PREFIX = "JunctionAgent";

    // The radius of junctions agents' knowledge. 1 is only adjacent streets
    public static final int JUNCTION_AGENT_RANGE = 3;

    // This many streets will be removed from agents knowledge
    // But only those that are in maximum range
    // and if JUNCTION_AGENT_RANGE > 1
    public static final int MISSING_KNOWLEDGE_SIZE = 2;

    // Number of ticks for a street event to persist
    public static final int MIN_EVENT_DURATION = 5;
    public static final int MAX_EVENT_DURATION = 20;

    // Number of ticks between each street event
    public static final int MIN_EVENT_PERIOD = 1;
    public static final int MAX_EVENT_PERIOD = 5;

    // Chance of street getting completely closed
    public static final double STREET_CLOSING_CHANCE = 0.7;

    // Traffic gain for each agent on a street
    public static final double AGENT_IMPACT_ON_TRAFFIC = 0.35;

    // Maximum traffic gain on a street
    public static final double MAX_TRAFFIC_GAIN = 2;

    // Identifiers of different communication protocols
    public static final String PROTOCOL_ROUTE_REQUEST = "RR";
    public static final String PROTOCOL_OBSTACLE_INFO = "CI";
    public static final String PROTOCOL_STREET_COST_REQUEST = "SCR";
    public static final String PROTOCOL_ENTER_STREET_INFO = "ESI";
    public static final String PROTOCOL_EXIT_STREET_INFO = "EXSI";
    public static final String PROTOCOL_ROUTE_FINISHED_INFO = "FRI";
}
