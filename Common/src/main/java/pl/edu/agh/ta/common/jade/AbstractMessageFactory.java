package pl.edu.agh.ta.common.jade;

import jade.lang.acl.ACLMessage;
import pl.edu.agh.ta.common.map.Route;

// Lets PC agents and mobile agents have distinct message passing implementations
public abstract class AbstractMessageFactory {
    public abstract ACLMessage createRouteRequest(int jctFrom, int jctTo);

    public abstract ACLMessage createRouteResponse(ACLMessage request, Route route);

    public abstract ACLMessage createStreetCostRequest(int jctFrom, int jctTo);

    public abstract ACLMessage createStreetCostResponse(ACLMessage request, int cost);

    public abstract ACLMessage createEnterStreetInfoMessage(int jctFrom, int jctTo);

    public abstract ACLMessage createExitStreetInfoMessage(int jctFrom, int jctTo);

    public abstract ACLMessage createRouteFinishedInfo();
}
