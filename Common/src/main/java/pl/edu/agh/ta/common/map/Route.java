package pl.edu.agh.ta.common.map;

import java.io.Serializable;
import java.util.LinkedList;

// Represents a route on map - list of junction numbers to visit
public class Route implements Serializable {
    private int startJunction;
    private LinkedList<Integer> route;
    private LinkedList<Integer> costs;

    public Route(int startJunction) {
        this.startJunction = startJunction;
        route = new LinkedList<Integer>();
        costs = new LinkedList<Integer>();
    }

    public void appendJunction(int jctNum, int cost) {
        route.add(jctNum);
        costs.add(cost);
    }

    public int getCost() {
        int cost = 0;
        for (Integer subCost : costs) {
            cost += subCost;
        }
        return cost;
    }

    public int getRouteCheckpointAt(int number) {
        return route.get(number);
    }

    public Route merge(Route subRoute) {
        Route result = new Route(startJunction);
        result.route = new LinkedList<Integer>(route);
        result.costs = new LinkedList<Integer>(costs);
        while (result.route.size() > 0 && result.route.getLast() != subRoute.startJunction) {
            result.route.removeLast();
            result.costs.removeLast();
        }
        if (result.route.size() == 0) {
            result = null;
        } else {
            for (int i = 0; i < subRoute.route.size(); i++) {
                result.route.add(subRoute.route.get(i));
                result.costs.add(subRoute.costs.get(i));
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Route [");
        sb.append(startJunction);
        sb.append(" -> ");
        sb.append(route.getLast());
        sb.append("], cost: ");
        sb.append(getCost());
        sb.append(", ");
        sb.append(startJunction);
        sb.append(" -> ");
        for (Integer jct : route) {
            sb.append(jct);
            sb.append(" -> ");
        }
        sb.delete(sb.length() - 4, sb.length() - 1);
        return sb.toString();
    }

    public String toString(int currentPos) {
        StringBuilder sb = new StringBuilder("Route [");
        sb.append(startJunction);
        sb.append(" -> ");
        sb.append(route.getLast());
        sb.append("], cost: ");
        sb.append(getCost());
        sb.append(", ");
        sb.append(startJunction);
        sb.append(" -> ");
        int i = 0;
        for (Integer jct : route) {
            sb.append(jct);
            if (i++ == currentPos) {
                sb.append(" [I'm here]");
            }
            sb.append(" -> ");
        }
        sb.delete(sb.length() - 4, sb.length() - 1);
        return sb.toString();
    }
}
