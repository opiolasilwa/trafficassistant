package pl.edu.agh.ta.common.map;

import java.util.*;
import java.util.List;

// Calculates shortest path from A -> B
public class AStarAlgorithm {
    public static Route getShortestRoute(StreetMap streetMap, int jctFrom, int jctTo) {
        double[] gScore = new double[streetMap.getNumberOfJunctions()];
        final double[] fScore = new double[streetMap.getNumberOfJunctions()];
        int[] cameFrom = new int[streetMap.getNumberOfJunctions()];

        PriorityQueue<Integer> openSet = new PriorityQueue<Integer>(streetMap.getNumberOfJunctions(), new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Double.compare(fScore[o1], fScore[o2]);
            }
        });

        List<Integer> closedSet = new LinkedList<Integer>();

        openSet.add(jctFrom);
        gScore[jctFrom] = 0;
        fScore[jctFrom] = streetMap.getJunctionLocation(jctFrom).distance(streetMap.getJunctionLocation(jctTo));
        cameFrom[jctFrom] = -1;

        int currentNode = -1;
        while (openSet.size() > 0) {
            // Get node with best fScore, remove it from the queue
            currentNode = openSet.poll();

            // If this node is the goal, finish
            if (currentNode == jctTo) {
                break;
            }

            // Add the node to closed set
            closedSet.add(currentNode);

            for (Integer neighbourId : streetMap.getNeighbours(currentNode)) {
                double currentGScore = gScore[currentNode] + streetMap.getStreetCost(currentNode, neighbourId);
                double distanceToGoal = streetMap.getJunctionLocation(neighbourId).distance(streetMap.getJunctionLocation(jctTo));

                if (!closedSet.contains(neighbourId)) {
                    boolean scoreBetter = false;
                    if (!openSet.contains(neighbourId)) {
                        openSet.add(neighbourId);
                        scoreBetter = true;
                    } else if (currentGScore < gScore[neighbourId]) {
                        scoreBetter = true;
                    }

                    if (scoreBetter) {
                        cameFrom[neighbourId] = currentNode;
                        gScore[neighbourId] = currentGScore;
                        fScore[neighbourId] = currentGScore + distanceToGoal;
                        // Refresh the distance in priority queue
                        openSet.remove(neighbourId);
                        openSet.add(neighbourId);
                    }
                }
            }
        }

        if (currentNode == jctTo) {
            List<Integer> reverseRoute = new LinkedList<Integer>();
            while (currentNode != jctFrom) {
                reverseRoute.add(currentNode);
                currentNode = cameFrom[currentNode];
            }

            Route result = new Route(jctFrom);
            Integer[] reverseRouteArray = reverseRoute.toArray(new Integer[reverseRoute.size()]);
            int lastJunction = jctFrom;
            for (int i = reverseRouteArray.length - 1; i >= 0; i--) {
                result.appendJunction(reverseRouteArray[i], streetMap.getStreetCost(lastJunction, reverseRouteArray[i]));
                lastJunction = reverseRouteArray[i];
            }

            return result;
        } else {
            return null;
        }
    }
}
